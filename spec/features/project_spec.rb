feature 'Project workflow', js: true do
  before :all do
    @home_page = HomePage.new
    @projects_page = ProjectsPage.new

    @user_name = 'test_user_' + Time.now.to_i.to_s
    @password = SecureRandom.base64 4

    @user_name1 = @user_name
    @password1 = @password
    @user_name2 = @user_name +'2'
    @password2 = @password
    @firstname1 = SecureRandom.base64 4
    @lastname1 = SecureRandom.base64 4
    @firstname2 = SecureRandom.base64 4
    @lastname2 = SecureRandom.base64 4
    @project_name = SecureRandom.hex(10).downcase
    @project_id = SecureRandom.hex(10).downcase
  end

  scenario 'User1 can create a Project' do

    register_user(@user_name1,@password1,@firstname1,@lastname1)
    @home_page.load
    @home_page.projects_btn.click
    expect(@projects_page).to be_displayed
    create_project(@project_name, @project_id)
    expect(@projects_page).to have_content 'Successful creation.'
    expect(@projects_page.projects_dropdown).to have_content @project_name
    logout_user
  end

  scenario 'User1 can assign new ticket to User2' do
    register_user(@user_name2,@password2,@firstname2,@lastname2)
    logout_user
    login_user(@user_name1,@password1)
    @projects_page.load
    add_member(@project_name,@firstname2,@lastname2)
    expect(@projects_page).to have_content @firstname2 + ' ' + @lastname2
    create_issue(@project_name,@firstname2,@lastname2)
    expect(@projects_page).to have_content 'New issue'
    expect(@projects_page).to have_content @firstname2 + ' ' + @lastname2
    logout_user
  end

  scenario 'User 2 can track time and delete the ticket' do
    login_user(@user_name2,@password2)
    @projects_page.load
    @hours = SecureRandom.random_number(24)
    track_time_and_delete(@project_name,@hours)
    sleep 2
    expect(@projects_page).to have_content @hours.to_s + '.00 hours'
    expect(@projects_page).to have_content @firstname2 + ' ' + @lastname2
    expect(@projects_page).to have_content 'Closed'
    logout_user
  end

  scenario 'User 1 can close the project' do
    login_user(@user_name1,@password1)
    @projects_page.load
    delete_project(@project_name)
    expect(@projects_page).to have_content 'This project is closed and read-only.'
    logout_user
  end

end
