feature 'Home page navigation', js: true do
  before :all do
    @home_page = HomePage.new
    @login_page = LoginPage.new
  end

  scenario 'User can successfully open homepage' do
    @home_page.load

    expect(@home_page).to be_displayed
    expect(@home_page).to have_content 'Redmine demo'
    expect(@home_page).to have_search_field
  end

  scenario 'User can successfully open Login page', js: true do
    @login_page.load

    expect(@login_page).to be_displayed
    expect(@login_page).to have_login_form
    expect(@login_page).to have_login_input
    expect(@login_page).to have_password_input
    expect(@login_page).to have_login_btn
    expect(@login_page).to have_register_btn
  end
end
