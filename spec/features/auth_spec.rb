feature 'User authentication', js: true do
before :all do
  @user_name = 'test_user_' + Time.now.to_i.to_s
  @password = SecureRandom.base64 4
  @home_page = HomePage.new
end

scenario 'Visitor can successfully sign up' do
    @firstname = 'Test'
    @lastname = 'User'
    register_user(@user_name,@password,@firstname,@lastname)

    expect(@home_page).to have_content 'Your account has been activated. You can now log in.'
    expect(@home_page).to have_top_menu
    expect(@home_page.top_menu).to have_content @user_name
    expect(@home_page).to have_account
    expect(@home_page).to have_logout_btn

end

  scenario 'Registered user can successfully sign in' do
   login_user(@user_name,@password)

   expect(@home_page.top_menu).to have_content @user_name
   expect(@home_page).to have_top_menu
   expect(@home_page).to have_logout_btn

  end

scenario 'Signed in user can successfully sign out' do
   login_user(@user_name,@password)

   logout_user

   expect(@home_page).to have_login_btn
   expect(@home_page).to have_register_btn
 end

end
