module FeatureHelper

  def register_user(user_name, password, firstname, lastname)

      @register_page = RegisterPage.new
      @register_page.load
      @register_page.user_login.set user_name
      @register_page.user_password.set password
      @register_page.user_password_confirmation.set password
      @register_page.user_firstname.set firstname
      @register_page.user_lastname.set lastname
      @register_page.user_mail.set user_name + '@gmail.com'
      @register_page.submit_btn.click
      sleep 0.5
    end

    def login_user(user_name,password)
      @login_page = LoginPage.new
      @login_page.load
      @login_page.login_input.set user_name
      @login_page.password_input.set password
      @login_page.login_btn.click
    end

    def logout_user
      @home_page = HomePage.new
      @home_page.load
      @home_page.logout_btn.click
    end

    def create_project(project_name,project_id)
      @projects_page = ProjectsPage.new
      @projects_page.new_project.click
      @projects_page.name_field.set project_name
      @projects_page.id_field.set project_id
      @projects_page.create_and_continue_btn.click
    end

    def add_member(project_name, firstname,lastname)
      @projects_page = ProjectsPage.new
      @home_page.projects_btn.click
      @projects_page.projects_dropdown.select(project_name)
      @projects_page.settings.click
      @projects_page.members.click
      @projects_page.new_member.click
      @projects_page.search_member.set (firstname + ' ' + lastname)
      sleep 0.5
      @projects_page.checkbox_member.click
      sleep 0.5
      @projects_page.role_developer.click
      @projects_page.add_btn.click
    end

    def create_issue(project_name, firstname, lastname)
      @projects_page = ProjectsPage.new
      @projects_page.projects_dropdown.select(project_name)
      @projects_page.new_issue.click
      sleep 0.5
      @projects_page.subject_field.set 'New issue'
      @projects_page.assignee.select(firstname + ' ' + lastname)
      @projects_page.create_issue_btn.click
    end

    def track_time_and_delete(project_name, hours)
      @projects_page = ProjectsPage.new
      @projects_page.projects_dropdown.select(project_name)
      @projects_page.issues_btn.click
      @projects_page.issues_link.right_click
      @projects_page.edit_issue_btn.click
      @projects_page.hours_field.set hours
      @projects_page.activity_option.select('Development')
      @projects_page.issue_status.select('Closed')
      @projects_page.submit_issue.click
    end

    def delete_project(project_name)
      @projects_page = ProjectsPage.new
      @projects_page.projects_dropdown.select(project_name)
      @projects_page.close_btn.click
      page.driver.browser.switch_to.alert.accept
    end

end
