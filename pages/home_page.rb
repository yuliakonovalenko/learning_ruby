class HomePage < SitePrism::Page
  element :logo, 'Redmine demo'
  element :login_btn, 'a.login'
  element :register_btn, 'a.register'
  element :top_menu, 'div#top-menu'
  element :account, 'a.my-account'
  element :logout_btn, :xpath, '//*[@id="account"]/ul/li[2]/a'
  element :search_field, 'input#q.small'
  element :projects_btn,:xpath, '//*[@id="top-menu"]/ul/li[3]/a'

  set_url "/"

end
