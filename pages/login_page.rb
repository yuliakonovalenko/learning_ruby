class LoginPage < SitePrism::Page
   element :top_menu, 'div#top-menu'
   element :login_form, :xpath, '//*[@id="login-form"]'
   element :login_input, 'input#username'
   element :password_input, 'input#password'
   element :login_btn, :xpath, '//*[@id="login-form"]/form/table/tbody/tr[4]/td[2]/input'
   element :register_btn, 'a.register'

   set_url "/login"
end
