class RegisterPage < SitePrism::Page
   element :user_login, '#user_login'
   element :user_password, '#user_password'
   element :user_password_confirmation, '#user_password_confirmation'
   element :user_firstname, '#user_firstname'
   element :user_lastname, '#user_lastname'
   element :user_mail, '#user_mail'
   element :submit_btn, :xpath, '//*[@id="new_user"]/input[3]'

   set_url "/account/register"

end
